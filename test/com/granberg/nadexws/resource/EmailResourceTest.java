/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.resource;

import com.granberg.model.EmailNotificationKey;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.json.JSONJAXBContext;
import com.sun.jersey.api.json.JSONMarshaller;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlo
 */
public class EmailResourceTest {

    public EmailResourceTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getXml method, of class EmailResource.
     */
    @Test
    public void testGetXml() {
    }

    /**
     * Test of notifyInstrument method, of class EmailResource.
     */
    @Test
    public void testNotifyInstrument() {
    }

    /**
     * Test of getEmail method, of class EmailResource.
     */
    @Test
    public void testGetEmail() {
        try {
            Client client = Client.create();
            WebResource resource = client.resource("http://localhost:8080/NadexWS/webresources/email");
//            WebResource resource = client.resource("http://tradingws.kyoobiq.com:8080/NadexWS/webresources/email");
            EmailNotificationKey emailNotification = new EmailNotificationKey();
            emailNotification.setInstrumentName("GBP/USD[Daily]");
            JSONJAXBContext ctx = new JSONJAXBContext(EmailNotificationKey.class);
            JSONMarshaller jsonM = ctx.createJSONMarshaller();
            StringWriter writer = new StringWriter();
            jsonM.marshallToJSON(emailNotification, writer);
            System.out.println(writer);
            String response = resource.path("findNewEmailTest").accept(MediaType.APPLICATION_JSON).entity(writer.toString(), MediaType.APPLICATION_JSON).post(String.class);
            System.out.println(response);
        } catch (JAXBException ex) {
            Logger.getLogger(EmailResourceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}