/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.resource;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.DataObject;
import com.granberg.model.Instrument;
import com.granberg.model.News;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.json.JSONJAXBContext;
import com.sun.jersey.api.json.JSONMarshaller;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author mdayacap
 */
public class NewsResourceTest {

    public NewsResourceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void notifyNewsWithinTimePeriod() {
        Client client = Client.create();
        WebResource resource = client.resource("http://localhost:8080/NadexWS/webresources/news");
//        WebResource resource = client.resource("http://tradingws.kyoobiq.com:8080/NadexWS/webresources/instrument");
        String reps = resource.path("/notifyNewsWithinTimePeriod/EUR%2FJPY").accept(MediaType.APPLICATION_JSON).put(String.class);
        System.out.println("RESP" + reps);

    }

    @Test
    public void invalidateNewsToNull() {
        Client client = Client.create();
//        WebResource resource = client.resource("http://localhost:8080/NadexWS/webresources/news");
        WebResource resource = client.resource("http://tradingws.kyoobiq.com:8080/NadexWS/webresources/news");
        resource.path("/invalidateNewsToNull/ES%2009-13").delete();
    }

    /**
     *
     * @throws JAXBException
     */
    @Test
    public void testNotifyInstrument() throws JAXBException {
        System.out.println("notifyInstrument");


        News news = new News();
        news.setCurrency("ES 09-13");
        news.setDate(new Date());
        news.setUpdatedDate(new Date());
        news.setImportance("High");
        news.setNewsId(1);
        Client client = Client.create();
//        WebResource resource = client.resource("http://localhost:8080/NadexWS/webresources/news");
        WebResource resource = client.resource("http://tradingws.kyoobiq.com:8080/NadexWS/webresources/news");
        JSONJAXBContext ctx = new JSONJAXBContext(News.class);
        JSONMarshaller jsonM = ctx.createJSONMarshaller();
        StringWriter writer = new StringWriter();
        jsonM.marshallToJSON(news, writer);
        System.out.println(writer);
        resource.path("notifyInstrument").accept(MediaType.APPLICATION_JSON).entity(writer.toString(), MediaType.APPLICATION_JSON).post();



    }

    /**
     *
     * @throws JAXBException
     */
    @Test
    public void testFindCurrentNews() throws JAXBException {
        System.out.println("testFindCurrentNews");
        Instrument instrument = new Instrument();
        instrument.setInstrumentName("GBP/JPY");



        Client client = Client.create();
//        WebResource resource = client.resource("http://localhost:8080/NadexWS/webresources/news");
        WebResource resource = client.resource("http://tradingws.kyoobiq.com:8080/NadexWS/webresources/news");
        JSONJAXBContext ctx = new JSONJAXBContext(Instrument.class);
        JSONMarshaller jsonM = ctx.createJSONMarshaller();
        StringWriter writer = new StringWriter();
        jsonM.marshallToJSON(instrument, writer);
        System.out.println(instrument.getInstrumentName());
        String response = resource.path("findCurrentNews").accept(MediaType.APPLICATION_JSON).entity(writer.toString(), MediaType.APPLICATION_JSON).post(String.class);
        System.out.println(response);
    }

    @Test
    public void testGetNewsWithinTimePeriod() throws JAXBException {
        System.out.println("getNewsWithinTimePeriod");
//        DataObject startAndEnd = new DataObject();
        Date startDate = new Date();
        long startMilliseconds = startDate.getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.add(Calendar.DATE, 4);
        Date endDate = cal.getTime();
        long endMilliseconds = endDate.getTime();
//        startAndEnd.setStartDate(startDate);
//        startAndEnd.setEndDate(endDate);
        Client client = Client.create();
        WebResource resource = client.resource("http://localhost:8080/NadexWS/webresources/news");
//        WebResource resource = client.resource("http://tradingws.kyoobiq.com:8080/NadexWS/webresources/news");
//        JSONJAXBContext ctx = new JSONJAXBContext(DataObject.class);
//        JSONMarshaller jsonM = ctx.createJSONMarshaller();
        StringWriter writer = new StringWriter();
//        jsonM.marshallToJSON(startAndEnd, writer);
        System.out.println(writer);
        String response = resource.path("getNewsWithinTimePeriod/" + startMilliseconds + "/" + endMilliseconds).accept(MediaType.APPLICATION_JSON).entity(writer.toString(), MediaType.APPLICATION_JSON).post(String.class);
        System.out.println(response);
    }

    @Test
    public void getAllNews(){
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        List<News> list = dBAccess.findNews();
        for (News news : list) {
            System.out.println( String.format("Date:%s %s", news.getCurrency(), news.getDate().toString()) );
        }        
    }
    
    @Test
    public void getoldNews() {
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        List<News> list = dBAccess.findNews();
        for (News news : list) {
//            Calendar cal = Calendar.getInstance();
//            cal.setTime(news.getDate());
//            cal.add(Calendar.MINUTE, 10);
//            Date sendDate = cal.getTime();
            Date newsDate = news.getDate();
            System.out.println(newsDate);
            Date actualDate = new Date();
            System.out.println(actualDate);
            long diff = actualDate.getTime() - newsDate.getTime();
            long diffInMinutes = diff / (1000 * 60);
            System.out.println(diffInMinutes);
//            long minutes = Math.abs(actualDate.getTime() - newsDate.getTime());
//            System.out.println(minutes);
//            if(actualDate.compareTo(newsDate) <= 10){
//                Logger.getLogger(NewsResourceTest.class.getName()).log(Level.INFO, "i'm here");
//            }
        }
    }
}
