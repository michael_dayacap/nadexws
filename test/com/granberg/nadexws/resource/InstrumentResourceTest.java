/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.resource;

import com.granberg.model.HourlyPips;
import com.granberg.model.HourlyPipsPK;
import com.granberg.model.Instrument;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.json.JSONJAXBContext;
import com.sun.jersey.api.json.JSONMarshaller;
import java.io.StringWriter;
import java.math.BigDecimal;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author clydemoreno
 */
public class InstrumentResourceTest {

    public InstrumentResourceTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getJson method, of class InstrumentResource.
     */
    @Test
    public void testGetJson() {
    }

    /**
     * Test of putJson method, of class InstrumentResource.
     */
    @Test
    public void testPutJson() {
    }

    /**
     *
     * @throws JAXBException
     */
    @Test
    public void TestGetHourlyPipsByInstrument() throws JAXBException {
        System.out.println("getHourlyPipsByInstrument");


        Instrument instrument = new Instrument();
        instrument.setInstrumentName("EUR/JPY");
        Client client = Client.create();
        WebResource resource = client.resource("http://localhost:8080/NadexWS/webresources/instrument");
        //WebResource resource = client.resource("http://tradingws.kyoobiq.com:8080/NadexWS/webresources/instrument");
        JSONJAXBContext ctx = new JSONJAXBContext(Instrument.class);
        JSONMarshaller jsonM = ctx.createJSONMarshaller();
        StringWriter writer = new StringWriter();
        jsonM.marshallToJSON(instrument, writer);
        System.out.println(writer);
        String response = resource.path("getHourlyPips").accept(MediaType.APPLICATION_JSON).entity(writer.toString(), MediaType.APPLICATION_JSON).post(String.class);
        System.out.println(response);


    }

    @Test
    public void testUpdateInstrumentDirect() {
        InstrumentResource r = new InstrumentResource();
        Instrument i = new Instrument();
        i.setInstrumentName("$EURJPY");
        i.setInstrumentPrice(BigDecimal.valueOf(140.009));
        r.updateInstrumentPrice(i);
    }

    /**
     * Test of updateInstrumentPrice method, of class InstrumentResource.
     */
    @Test
    public void testUpdateInstrumentPrice_Instrument() throws JAXBException {
        System.out.println("updateInstrumentPrice");
        Instrument instrument = new Instrument();
        instrument.setInstrumentName("$EURUSD");
        instrument.setInstrumentPrice(BigDecimal.valueOf(140.009));
        Client client = Client.create();
        //WebResource resource = client.resource("http://localhost:8080/NadexWS/webresources/instrument");
        WebResource resource = client.resource("http://tradingws.kyoobiq.com:8080/NadexWS/webresources/instrument");
        JSONJAXBContext ctx = new JSONJAXBContext(Instrument.class);
        JSONMarshaller jsonM = ctx.createJSONMarshaller();
        StringWriter writer = new StringWriter();
        jsonM.marshallToJSON(instrument, writer);
        System.out.println(writer);
        resource.path("updatedTrend").accept(MediaType.APPLICATION_JSON).entity(writer.toString(), MediaType.APPLICATION_JSON).post();
    }

    @Test
    public void getHourlyPips() throws JAXBException {
        Client client = Client.create();
        WebResource resource = client.resource("http://localhost:8080/NadexWS/webresources/instrument");
        HourlyPips hourlyPips = new HourlyPips();
        HourlyPipsPK hpPk = new HourlyPipsPK();
        hpPk.setInstrumentName("EUR/JPY");
        hpPk.setMovingAverageInDays(15);
        hourlyPips.setHourlyInterval(4);
        hourlyPips.setHourlyPipsPK(hpPk);
        JSONJAXBContext ctx = new JSONJAXBContext(HourlyPips.class);
        JSONMarshaller jsonM = ctx.createJSONMarshaller();
        StringWriter writer = new StringWriter();
        jsonM.marshallToJSON(hourlyPips, writer);
        System.out.println("REQ: " + writer);

        String resp = resource.path("getHourlyPips").accept(MediaType.APPLICATION_JSON).entity(writer.toString(), MediaType.APPLICATION_JSON).post(String.class);
        System.out.println(resp);
    }
}