/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.resource;

import com.granberg.model.Trend;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.json.JSONJAXBContext;
import com.sun.jersey.api.json.JSONMarshaller;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Carlo
 */
public class TrendResourceTest {

    public TrendResourceTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getJson method, of class TrendResource.
     */
    @Test
    public void testGetJson() {
    }

    /**
     * Test of putJson method, of class TrendResource.
     */
    @Test
    public void testPutJson() {
    }

    /**
     * Test of getAllTrends method, of class TrendResource.
     */
    @Test
    public void testGetAllTrends() {
        Client client = Client.create();
        WebResource resource = client.resource("http://localhost:8080/NadexWS/webresources/trend");
        //                WebResource resource = client.resource("http://tradingws.kyoobiq.com:8080/NadexWS/webresources/news");
//            JSONJAXBContext ctx = new JSONJAXBContext(News.class);
//            JSONMarshaller jsonM = ctx.createJSONMarshaller();
        StringWriter writer = new StringWriter();
//            jsonM.marshallToJSON(json, writer);
        System.out.println(writer);
        String response = resource.path("allTrends").accept(MediaType.APPLICATION_JSON).entity(writer.toString(), MediaType.APPLICATION_JSON).post(String.class);
        System.out.println(response);
    }

    /**
     * Test of updatedTend method, of class TrendResource.
     */
    @Test
    public void testUpdatedTend() {
        try {
            Trend trend = new Trend();
            trend.setTrendId(1);
            trend.setInsturmentName("EUR/JPY");
            trend.setTimeframeInMinutes(5);
            trend.setUpdatedOn(new Date());
            trend.setBias("long");
            Client client = Client.create();
            WebResource resource = client.resource("http://localhost:8080/NadexWS/webresources/trend");
            JSONJAXBContext ctx = new JSONJAXBContext(Trend.class);
            JSONMarshaller jsonM = ctx.createJSONMarshaller();
            StringWriter writer = new StringWriter();
            jsonM.marshallToJSON(trend, writer);
            System.out.println(writer);
            resource.path("updateTrend").accept(MediaType.APPLICATION_JSON).entity(writer.toString(), MediaType.APPLICATION_JSON).post();
        } catch (JAXBException ex) {
            Logger.getLogger(TrendResourceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}