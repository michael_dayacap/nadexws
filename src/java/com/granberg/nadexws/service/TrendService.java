/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.service;

import com.granberg.model.Trend;
import java.util.List;

/**
 *
 * @author Carlo
 */
public interface TrendService {

    List<Trend> getAllTrends();
    void updateTrend(Trend trend);
}
