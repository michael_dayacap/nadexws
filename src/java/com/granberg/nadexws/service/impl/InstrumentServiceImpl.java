/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.service.impl;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.HourlyPips;
import com.granberg.nadexws.service.InstrumentService;
import java.util.List;

/**
 *
 * @author mdayacap
 */
public class InstrumentServiceImpl implements InstrumentService {

        private static final IDBAccess dbAccess = MySQLDBAccess.getInstance();

    
    @Override
    public List<HourlyPips> getHourlyPips(String instrumentName, Integer movingAverageInDays, Integer hourlyInterval) {
        return dbAccess.getHourlyPips(instrumentName, movingAverageInDays, hourlyInterval);
    }
    
}
