package com.granberg.nadexws.service.impl;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.News;
import com.granberg.nadexws.service.NewsService;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mdayacap
 */
// TODO Implement @Transaction
public class NewsServiceImpl implements NewsService {

    private static final IDBAccess dbAccess = MySQLDBAccess.getInstance();
      
    @Override
    public List<News> getAllNews() {
        return dbAccess.findNews();
    }

    @Override
    public List<News> findNewsByWithinPeriod(Date startDate, Date endDate) {
        return dbAccess.findNewsByWithinPeriod(startDate, endDate);
    }
 
    @Override
    public News findCurrentNews(String instrumentName) {
            //singleton.
        //servletcontext.
        //return singleton.hashmap[instrumentName];
        News news = new News();
        news.setImportance("High");
        news.setEvents("Some News");
        news.setUpdatedDate(new Date());
        news.setCurrency("EUR/JPY");
        return news;
    }
     @Override
    public void invalidateCurrentNews(String instrumentName) {
            //singleton.
        //servletcontext.
        //singleton.hashmap[instrumentName] = null;
        
    }
}
