/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.service.impl;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.Trend;
import com.granberg.nadexws.service.TrendService;
import java.util.List;

/**
 *
 * @author Carlo
 */
public class TrendServiceImpl implements TrendService {

    private static final IDBAccess dbAccess = MySQLDBAccess.getInstance();

    @Override
    public List<Trend> getAllTrends() {
        return dbAccess.findAllTrends();
    }

    @Override
    public void updateTrend(Trend trend) {
        dbAccess.updateTrend(trend);
    }
}
