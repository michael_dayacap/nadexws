/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.service;

import com.granberg.model.News;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mdayacap
 */
public interface NewsService {
    
    List<News> getAllNews();

    List<News> findNewsByWithinPeriod(Date startDate, Date endDate);
    News findCurrentNews(String instrumentName);
    void invalidateCurrentNews(String instrumentName); //
    
}
