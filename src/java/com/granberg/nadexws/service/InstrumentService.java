/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.service;

import com.granberg.model.HourlyPips;
import java.util.List;

/**
 *
 * @author mdayacap
 */
public interface InstrumentService {

    List<HourlyPips> getHourlyPips(String instrumentName, Integer averageType, Integer hourlyInterval);
    
}
