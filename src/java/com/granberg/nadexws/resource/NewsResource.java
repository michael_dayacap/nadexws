/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.resource;

import com.granberg.model.DataObject;
import com.granberg.model.Instrument;
import com.granberg.nadexws.service.NewsService;
import com.granberg.model.News;
import com.granberg.nadexws.service.impl.NewsServiceImpl;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.Context;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author mdayacap
 */
@Path("news")
public class NewsResource {

    @Context
    private ServletContext context;
    private NewsService newsService;

    /**
     * Creates a new instance of NewsResource
     */
    public NewsResource() {
        newsService = new NewsServiceImpl();
    }

    /**
     * Retrieves all News
     *
     * @return an instance of com.granberg.model.News
     */
    @GET
    @Produces("application/json")
    @Path("/all")
    public List<News> getAllNews() {
        return newsService.getAllNews();
    }

    /**
     * Find News within a period of time
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @GET
    @Produces("application/json")
    @Path("/findNewsByWithinPeriod/{startTimeInMilliSec}/{endTimeInMilliSec}")
    public List<News> findNewsByWithinPeriod(@PathParam("startTimeInMilliSec") String startTime, @PathParam("endTimeInMilliSec") String endTime) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        cal.setTimeInMillis(Long.parseLong(startTime));
        Date startDate = cal.getTime();
        cal.setTimeInMillis(Long.parseLong(endTime));
        Date endDate = cal.getTime();
        return newsService.findNewsByWithinPeriod(startDate, endDate);

    }

    /**
     *
     * @param instrumentName
     * @return
     */
    @PUT
    @Path("/notifyNewsWithinTimePeriod/{instrumentName}")
    @Produces(MediaType.APPLICATION_JSON)
    public News notifyNewsWithinTimePeriod(@PathParam("instrumentName") String instrumentName) {
        Map<String, News> map = (Map<String, News>) context.getAttribute("instrumentNewsMap");
        News news = map.get(instrumentName);
        return news;
    }

    /**
     *
     * @param instrument
     */
    @POST
    @Consumes("application/json")
    @Path("/notifyInstrument")
    public void notifyInstrument(News news) {
        Map<String, News> map = (Map<String, News>) context.getAttribute("instrumentNewsMap");
        map.put(news.getCurrency(), news);
    }

    @DELETE
    @Path("/invalidateNewsToNull/{instrumentName}")
    public void invalidateNewsToNull(@PathParam("instrumentName") String instrumentName) {
        Map<String, News> map = (Map<String, News>) context.getAttribute("instrumentNewsMap");
        map.put(instrumentName, new News());
    }

    @POST
    @Path("/findCurrentNews/{instrumentName}")
    @Produces(MediaType.APPLICATION_JSON)
    public News findCurrentNewsByInstrumentName(@PathParam("instrumentName") String instrumentName) {
        return newsService.findCurrentNews(instrumentName);
    }

    @POST
    @Produces("application/json")
    @Path("/getNewsWithinTimePeriod/{startdate}/{enddate}")
    public List<News> getNewsWithinTimePeriod(@PathParam("startdate") String startTime, @PathParam("enddate") String endTime) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        cal.setTimeInMillis(Long.parseLong(startTime));
        Date startDate = cal.getTime();
        cal.setTimeInMillis(Long.parseLong(endTime));
        Date endDate = cal.getTime();
        return newsService.findNewsByWithinPeriod(startDate, endDate);
    }

    @POST
    @Path("/findCurrentNews")
    @Produces(MediaType.APPLICATION_JSON)
    public News findCurrentNews(Instrument instrument) {
//        Instrument i = new Instrument();
//        i.setInstrumentName(instrument.getInstrumentName());
//        
//        News n = new News();
//        n.setCurrency(instrument.getInstrumentName());
//        n.setEvents("Some News");
//        return n;
        Map<String, News> map = (Map<String, News>) context.getAttribute("instrumentNewsMap");
        News news = map.get(instrument.getInstrumentName());
        if (news == null) {
            news = new News();
        } else {
            Date actualDate = new Date();
            long diff = actualDate.getTime() - news.getDate().getTime();
            long diffInMinutes = diff / (1000 * 60);
            //check if news date is 10 minutes over the actual date
            //if it is, then set the map to null and return 
            //return a new News object
            if (diffInMinutes >= 10) {
                news = new News();
            }
        }
        return news;
    }
}