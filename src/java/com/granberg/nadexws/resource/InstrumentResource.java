/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.resource;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.HourlyPips;
import com.granberg.model.Instrument;
import com.granberg.model.InstrumentMapping;
import com.granberg.nadexws.service.InstrumentService;
import com.granberg.nadexws.service.impl.InstrumentServiceImpl;
import java.math.BigDecimal;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author clydemoreno
 */
@Path("instrument")
public class InstrumentResource {

    @Context
    private UriInfo context;
    private InstrumentService instrumentService;

    /**
     * Creates a new instance of InstrumentResource
     */
    public InstrumentResource() {
        instrumentService = new InstrumentServiceImpl();
    }

    /**
     * Retrieves representation of an instance of
     * com.granberg.nadexws.resource.InstrumentResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of InstrumentResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }

    /**
     * PUT method for updating or creating an instance of InstrumentResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    @Path("/instrument/{instrumentName}/{price}")
    //@PathParam("startTimeInMilliSec") String startTime, @PathParam("endTimeInMilliSec") String endTime
    public void updateInstrumentPrice(@PathParam("instrumentName") String instrumentName, @PathParam("price") BigDecimal price) {
        IDBAccess dbAccess = MySQLDBAccess.getInstance();
        dbAccess.updateInstrumentPrice(instrumentName, price);
    }

    /**
     *
     * @param instrument
     */
    @POST
    @Consumes("application/json")
    @Path("/updateInstrument")
    //@PathParam("startTimeInMilliSec") String startTime, @PathParam("endTimeInMilliSec") String endTime
    public void updateInstrumentPrice(Instrument instrument) {
        IDBAccess dbAccess = MySQLDBAccess.getInstance();
        String instrumentName = "";
        Instrument existingInstrument = null; 
        try{
            dbAccess.findInstrumentByName(instrument.getInstrumentName());
        }
        catch(Exception ex){
            // do something
        }
        InstrumentMapping instrumentMapping;
        if(existingInstrument == null){
            try{
             instrumentMapping = dbAccess.findInstrumentByAlternateName(instrument.getInstrumentName());
             instrumentName = instrumentMapping.getInstrumentName();
            }
            catch(Exception lex){
                // do s
            }
        }
        else{
            instrumentName = existingInstrument.getInstrumentName();
        
        }
        dbAccess.updateInstrumentPrice(instrumentName, instrument.getInstrumentPrice());

        //dbAccess.updateInstrumentPrice(instrument.getInstrumentName(), instrument.getInstrumentPrice());
    }

    /**
     *
     * @param instrument
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getHourlyPips")
    public List<HourlyPips> getHourlyPips(HourlyPips hourlyPips) {

        return instrumentService.getHourlyPips(hourlyPips.getHourlyPipsPK().getInstrumentName(), hourlyPips.getHourlyPipsPK().getMovingAverageInDays(), hourlyPips.getHourlyInterval());
    }
}
