/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.resource;

import com.granberg.model.EmailNotification;
import com.granberg.model.EmailNotificationKey;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author Carlo
 */
@Path("email")
public class EmailResource {

    @Context
    private ServletContext context;

    /**
     * Creates a new instance of EmailResource
     */
    public EmailResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.granberg.nadexws.resource.EmailResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/xml")
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param instrument
     */
    @POST
    @Consumes("application/json")
    @Path("/notifyEmail/")
    public void notifyInstrument(EmailNotification emailNotification) {
        Map<String, EmailNotification> map = (Map<String, EmailNotification>) context.getAttribute("emailMap");
        map.put(emailNotification.getInstrumentName(), emailNotification);
    }

    @POST
    @Consumes("application/json")
    @Path("/notifyEmailTest/{notification}")
    public void notifyInstrumentTest(@PathParam("notification") String email) {
        List<String> map = (List<String>) context.getAttribute("emailNotification");
        map.add(email);
    }

//    @POST
//    @Produces("application/json")
//    @Path("/findNewEmail/")
//    public EmailNotification getEmail(EmailNotification emailNotification) {
//        Map<String, EmailNotification> map = (Map<String, EmailNotification>) context.getAttribute("emailMap");
//        EmailNotification notification = map.get(emailNotification.getInstrumentName());
//        if (notification == null) {
//            notification = new EmailNotification();
//        } else {
//            Date actualDate = new Date();
//            long diff = actualDate.getTime() - emailNotification.getDate().getTime();
//            long diffInMinutes = diff / (1000 * 60);
//            if (diffInMinutes >= 3) {
//                notification = new EmailNotification();
//            }
//        }
//        return notification;
//    }
    
    @POST
    @Produces("application/json")
    @Path("/findNewEmail/")
    public EmailNotification getEmail(EmailNotificationKey emailNotificationKey) {
        Map<String, EmailNotification> map = (Map<String, EmailNotification>) context.getAttribute("emailMap");
        EmailNotification notification = map.get(emailNotificationKey.getInstrumentName());
        if (notification == null) {
            notification = new EmailNotification();
        } else {
            Date actualDate = new Date();
            long diff = actualDate.getTime() - notification.getDate().getTime();
            long diffInMinutes = diff / (1000 * 60);
            if (diffInMinutes >= 3) {
                notification = new EmailNotification();
            }
        }
        return notification;
    }
}
