/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.resource;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.Trend;
import com.granberg.nadexws.service.TrendService;
import com.granberg.nadexws.service.impl.TrendServiceImpl;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author Carlo
 */
@Path("trend")
public class TrendResource {

    @Context
    private ServletContext context;
    private TrendService trendService;

    /**
     * Creates a new instance of TrendResource
     */
    public TrendResource() {
        trendService = new TrendServiceImpl();
    }

    /**
     * Retrieves representation of an instance of com.granberg.nadexws.resource.TrendResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of TrendResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }
    
    @POST
    @Produces("application/json")
    @Path("/allTrends")
    public List<Trend> getAllTrends(){
        return trendService.getAllTrends();
    }
    
    @POST
    @Consumes("application/json")
    @Path("/updateTrend")
    public void updatedTend(Trend trend){
        trendService.updateTrend(trend);
    }
}
