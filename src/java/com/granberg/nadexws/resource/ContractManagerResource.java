/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws.resource;

import com.granberg.model.Contract;
import com.granberg.model.Instrument;
import com.granberg.strategy.ContractManager;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Carlo
 */
@Path("contract")
public class ContractManagerResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ContractManagerResource
     */
    public ContractManagerResource() {
    }

    /**
     * Retrieves representation of an instance of com.granberg.nadexws.resource.ContractManagerResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of ContractManagerResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }
    
    @POST
    @Path("/findNearestActiveContract")
    @Produces("application/json")
    public List<Contract> getNearestActiveContract(Instrument instrument){
        ContractManager contractManager = ContractManager.getInstance();
        return contractManager.getNearestActiveContract(instrument);
    }
    
    @POST
    @Path("/findContractInstrumentPrice")
    @Produces("application/json")
    public Instrument getCurrentContractPrice(Contract contract){
        ContractManager contractManager = ContractManager.getInstance();
        return contractManager.getCurrentContractPrice(contract);
    }
    
    @POST
    @Path("/findTimeLeftForContactInMinutes")
    @Produces("application/json")
    public long getTimeLeftForContactInMinutes(Contract contract){
        ContractManager contractManager = ContractManager.getInstance();
        return contractManager.getTimeLeftForContactInMinutes(contract);
    }
}
