/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.nadexws;

import com.granberg.model.EmailNotification;
import com.granberg.model.News;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author mdayacap
 */
public class NadexWSServletContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        Map<String, News> concurrentMap = Collections.synchronizedMap(new HashMap<String, News>());
        sce.getServletContext().setAttribute("instrumentNewsMap", concurrentMap);
        Map<String, EmailNotification> concurrentMaptest = Collections.synchronizedMap(new HashMap<String, EmailNotification>());
        sce.getServletContext().setAttribute("emailMap", concurrentMaptest);
        List<String> notificationCollection = new ArrayList<String>();
        sce.getServletContext().setAttribute("emailNotification", notificationCollection);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        // do nothing
    }
}
